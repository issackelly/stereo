#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='Stereo',
    version = '0.1',
    packages = find_packages(),
    options = {
        'app': {
            'formal_name': 'Stereo',
            'bundle': 'com.issackelly.stereo'
        },
        'macos': {
            'app_requires': [
                'toga',
                'rxv',
                'urllib3',
                'zlib',
            ]
        }
    }
)
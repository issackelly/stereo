#!/usr/bin/env python

import toga
import rxv

receivers = rxv.find()
rx = receivers[0]
last_volume = rx.volume

def mute(widget):
    if rx.mute:
        rx.mute = False
    else:
        rx.mute = True

def vol_down(widget):
    rx.volume -= 1

def vol_up(widget):
    rx.volume += 1

def pc(widget):
    rx.input = "HDMI4"

def sonos(widget):
    rx.input = "AV1"


def build(app):
    box = toga.Box()

    down_button = toga.Button('-', on_press=vol_down)
    down_button.style.set(margin=3)
    box.add(down_button)

    up_button = toga.Button('+', on_press=vol_up)
    up_button.style.set(margin=3)
    box.add(up_button)

    sonos_button = toga.Button('sonos', on_press=sonos)
    sonos_button.style.set(margin=3)
    box.add(sonos_button)

    pc_button = toga.Button('pc', on_press=pc)
    pc_button.style.set(margin=3)
    box.add(pc_button)


    mute_button = toga.Button('mute', on_press=mute)
    mute_button.style.set(margin=3)
    box.add(mute_button)

    return box

app = toga.App('Stereo', 'com.issackelly.stereo', startup=build)
app.main_loop()